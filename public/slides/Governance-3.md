## Elements of FLOSS Governance


- Ownership of assets​

- Chartering the project​

- Community management​

- Software development processes​

- Conflict resolution and rule changing​

- Use of information and tools

\
[Markus, M.Lynne 2007](https://link.springer.com/article/10.1007/s10997-007-9021-x)​
## Overview of common Tools

- **Version Control Systems:** git, Apache Subversion, Mercurial, GNU CVS, etc
- **Code Review Tools​:** email, Gerrit, Phabricator, Gitlab, Github
- **Code Release or Hosting​:** Gitlab​, Bitbucket, GitHub
## BSD-3 Clause
> Can ✔ - Cannot ❌ - Must ❕

- Private Use ✔
- Modification ✔
- Distribution ✔*
- Sublicensing ✔*
- Patent grant ✔
- Trademark grant ✔

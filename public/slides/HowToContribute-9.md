


#### Be Patient
#### Respect community decisions and culture
#### Be patient

See more guides here:
- <https://opensource.guide/how-to-contribute/>

Find ***Good First Issues*** here:
- [https://goodfirstissues.com](https://goodfirstissues.com)
- [https://www.firsttimersonly.com](https://www.firsttimersonly.com)
- [https://github.com/firstcontributions/first-contributions](https://github.com/firstcontributions/first-contributions)
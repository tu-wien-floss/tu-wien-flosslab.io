# Licenses

> **Disclaimer: Not Legal Counsel On Property Law ⚖️** 

Intellectual property laws **forbid the use** of work **without the consent** of the creator.*

Licenses **permit the user** to engage in an activity which is illegal, and subject to prosecution otherwise.
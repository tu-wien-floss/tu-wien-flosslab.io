## What is FLOSS governance?​

>... direction, control, and coordination of 
wholly or partially 
autonomous individuals and organizations on behalf
of an OSS development project to which they jointly contribute.

\
[Markus, M.Lynne 2007](https://link.springer.com/article/10.1007/s10997-007-9021-x)​

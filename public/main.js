let md = window.markdownit();
let slideConfig = window.slideConfig;

/**
 * Fetches a slide and transforms it to HTML in a string
 * @param {string} fileName - the filename of the slide to render e.g. "history.md"
 * @returns {string} - the markdown content of the file transformed to HTML
 */
async function renderSlide(fileName) {
  try {
    const result = await fetch(`slides/${fileName}`, { mode: "no-cors" });
    const slideContent = await result.text();
    if (fileName.endsWith('md')) {
      return md.render(slideContent);
    }
    return slideContent;
  } catch (e) {
    console.error(e);
  }
}

/**
 * Transforms all passed slides to HTML in strings
 * @param {string[]} slides - the slides to render
 * @returns {string[]} - the slides transformed to HTML strings
 */
async function renderSlides(slides) {
  return Promise.all(
    slides.map(function (slide) {
      return renderSlide(slide);
    })
  );
}

// Take all slides in the slideConfig and render them to the page as
// presentation slides
renderSlides(slideConfig).then((renderedSlides) => {
  const presentation = document.getElementById("slides");
  renderedSlides.forEach(function (slideHTML) {
    const slideWrapper = document.createElement("section");
    slideWrapper.innerHTML = slideHTML;
    presentation.appendChild(slideWrapper);
  });
});

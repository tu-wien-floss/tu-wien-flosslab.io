## tu-wien-floss.gitlab.io
A presentation about free/libre and open source software (FLOSS).

## Motivation
This project was created as a place to practice contributing to open source by collaboratively creating a presentation.

## Deployment
The `master` branch of this repository is continuously built and deployed to [tu-wien-floss.gitlab.io](https://tu-wien-floss.gitlab.io/).

## Technology Stack

This project uses
- [reveal.js](https://revealjs.com/) to create a presentation that can be viewed in a web browser
- [markdown-it](https://github.com/markdown-it/markdown-it) to transform markdown slides into HTML
- HTML and Markdown to create presentation slides. [This cheat sheet](https://www.markdownguide.org/cheat-sheet/) provides a quick overview over the Markdown syntax.


## Usage
This section explains how to set up your local environment and start working on the project.

### Download

To start working on this project, you need to clone this repository using git. Do you have SSH installed?

**YES:**

Please add the SSH public key to your profiles verified SSH keys.
Open a shell, navigate to your desired download location and enter:

`git clone git@gitlab.com:tu-wien-floss/tu-wien-floss.gitlab.io.git`

**NO:**
Open a shell, navigate to your desired download location and enter:

`git clone https://gitlab.com/tu-wien-floss/tu-wien-floss.gitlab.io.git`


### Running the Presentation

The project is a web application which needs to be served via a web server.
Do you have a web server application installed?

**YES:**

Very good. Continue at "Starting the Web Server"

**NO:**

[Please find a http server for your prefered language.](https://github.com/imgarylai/awesome-webservers)

If you feel lost by now, you might have to install some additional software to solve your problem. Why not try [Node.js](https://nodejs.org/en/download/)?

nodejs brings along a package manager, npm, featuring *over a million* packages, for instance executable web servers:

`npm install --global http-server`


#### Starting the Web Server

Now, please start your web server and let it serve the content from the projects `public` directory. 
The web server application tells you which `PORT` it uses, if you do not specify it on startup.
Open a browser and navigate to `localhost:PORT` to view the presentation.

Python comes equipped with an integrated webserver:

**Python 2**

`python -m SimpleHTTPServer 8000`

**Python 3**

`python -m http.server 8000`

Starting the previously installed http-server:

`http-server -p 8000`

Using one the above commands, the website should be served at localhost:8000. Once loaded, you may switch slides by using your arrow keys and reach an overview with `ESC`.

### Creating Slides
Slides are located in `public/slides`. Slides can be created as HTML or Markdown files. If you want to create a new slide, just create a new file in this directory.

**Markdown example:**

`MySlide.md`

```
## Slide Title
- This is a bullet point
- This is another bullet point
```

**HTML example:**

`MySlide.html`
```
<h2>Slide Title</h2>
<ul>
  <li>This is a bullet point</li>
  <li>This is another bullet point</li>
</ul>

```

### Adding images
If you want to add an image to your slide, place it in `public/images`. You can then reference it from your slide files. Don't forget to prefix the filename with `images/`.

**Markdown example**
```
![A photo of a cat](images/cat.jpg)
```

**HTML example**

When using HTML you should add a `width` and `height` to your image.
```
<img src="images/cat.jpg" alt="A photo of a cat" width="300" height="364">
```

### Adding Custom CSS
When creating HTML slides you can add custom CSS. Add class names to the elements you would like to style and then create style rules for them in `customStyles.css`.

**Example**

In `MySlide.html`
```
<h2>My Slide</h2>
<div class="side-by-side">
  <p>Some text here...</p>
  <img src="images/cat.jpg" alt="A photo of a cat" width="300" height="364">
</div>
```

In `customStyles.css` add
```
.side-by-side {
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-around;
}
```

### Configuring Slides
To show, hide and order the slides of the presentation you need to edit `slideConfig.js`. List all slides in the order you want them to appear in the presentation. They need to be separated by commas.

```
window.slideConfig = [
  "intro.md",
  "an-html-slide.html",
  "the-third-slide.html",
  "last-slide.md",
];
```

## Contribute

To learn how you can contribute to this project, please refer to the [CONTRIBUTING.md](https://gitlab.com/tu-wien-floss/tu-wien-floss.gitlab.io/-/blob/master/CONTRIBUTING.md).

## Notes
This project was created for a session in the Free and Open Technologies class at TUWien (summer term 21).


## License
//TODO
